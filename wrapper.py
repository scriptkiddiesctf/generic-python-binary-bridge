from subprocess import Popen,PIPE
import sys
import threading
import os
import time


p1 = Popen('./tweety_bird',stdin=PIPE,stdout=sys.stdout,stderr=PIPE)


NOT_ALLOWED_DATA = [

] # Todo figure if I need lower values
break_point = False
MAX_TIME_OUT_TICK = 60
NO_READ = False 

def str_comp_within(line,str_arr):
    return True if len([sub_str for sub_str in str_arr if sub_str in line]) > 0 else False

def time_out_thread(): # $ this tread is created to 
    global break_point
    global MAX_TIME_OUT_TICK
    global NO_READ
    while True:
        time.sleep(MAX_TIME_OUT_TICK)
        if NO_READ:
            NO_READ = False
        else:
            break_point = True
                        
def read_thread():
    global p1
    global break_point
    while True:
        for line in iter(sys.stdin.readline,""):
            if str_comp_within(line.rstrip(),NOT_ALLOWED_DATA):
                break_point = True
                break
            NO_READ = True
            p1.stdin.write(line)
            p1.stdin.flush()

def poll_checker():
    global p1
    global break_point
    while True:
        if p1.poll()!= None or break_point:
            break

if __name__ == '__main__':
    read_thread_obj = threading.Thread(target=read_thread)
    poll_thread_obj = threading.Thread(target=poll_checker)
    time_out_thread_obj = threading.Thread(target=time_out_thread)
    # write_thread_obj = threading.Thread(target=write_thread)
    read_thread_obj.setDaemon(True)
    time_out_thread_obj.setDaemon(True)
    # write_thread_obj.start()
    read_thread_obj.start()
    time_out_thread_obj.start()
    poll_thread_obj.start()
    poll_thread_obj.join()