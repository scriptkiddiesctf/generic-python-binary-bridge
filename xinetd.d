service tweety_bird
{
    socket_type = stream
    protocol    = tcp
    wait        = no
    user        = ctf_tweety_bird
    bind        = 0.0.0.0
    server      = /usr/bin/python
    server_args = /opt/ctf/tweety_bird/ro/wrapper.py
    port        = 20003
    type        = UNLISTED
    instances   = 1000
}