# Binary Bridge with Python

## Files

- ``xinetd.d`` : This file contains the configuration that needs to be deployed in the CTF box to use the bridge instead of the binary. 

- ``deploy.sh`` : This is small snippet of the deployment script that would be used when a patch is deployed on the CTF machine. **Please deploy using root**

- ``wrapper.py`` : The actual bridge to the binary. Uses three threads :
    - ``read_thread`` : thread will read from ``sys.stdin``
    - ``poll_checker`` : Thread will check if the program has died because of some error. 
    - ``time_out_thread`` : This thread was created to kill the service if it remain inactive for a certain period of time. This was done because if user cuts the connections to our service without closing the connection then the spawned python service is not killed and stays in the backgroud. Without this our patches hogged a lot of memory in our pervious CTF. 
    - There is no thread for stdout because we can directly pipe that to sys.stdout without the need of seperate thread.  

## Author 
- [Valay Dave](vddave@asu.edu)